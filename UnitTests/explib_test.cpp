#include "stdafx.h"
#include "CppUnitTest.h"
#include <explib/Explib.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(ExpLibTest)
	{
	public:
		
		TEST_METHOD(SingleSymbol)
		{
			Assert::AreEqual(explib::sortExpression("A"), std::string("A"));
		}

		TEST_METHOD(SimpleBrackets)
		{
			Assert::AreEqual(explib::sortExpression("(A + D)"), std::string("(A + D)"));
		}

		TEST_METHOD(LostFormattedSpace)
		{
			Assert::AreEqual(explib::sortExpression("(A +  D)"), std::string("(A + D)"));
		}

		TEST_METHOD(SimpleSorting)
		{
			Assert::AreEqual(explib::sortExpression("(D + A)"), std::string("(A + D)"));
		}

		TEST_METHOD(SimpleSort)
		{
			Assert::AreEqual(explib::sortExpression("(D + A)"), std::string("(A + D)"));
		}

		TEST_METHOD(SimpleSortWithBrackets)
		{
			Assert::AreEqual(explib::sortExpression("(D + (A + B))"), std::string("((A + B) + D)"));
		}

		TEST_METHOD(RecursiveSortWithBrackets)
		{
			Assert::AreEqual(explib::sortExpression("(D + (B + A))"), std::string("((A + B) + D)"));
		}

		TEST_METHOD(LongElementsString)
		{
			Assert::AreEqual(explib::sortExpression("(D + (B + A) + C + K + F + J + (E + S))"), std::string("((A + B) + C + D + (E + S) + F + J + K)"));
		}

		TEST_METHOD(TwoPicks)
		{
			Assert::AreEqual(explib::sortExpression("((D + (B + A)) + ((D + S) + (U + E)))"), std::string("(((A + B) + D) + ((D + S) + (E + U)))"));
		}

		TEST_METHOD(DeepBrackets)
		{
			Assert::AreEqual(explib::sortExpression("(((A)))"), std::string("A"));
		}

		TEST_METHOD(RemoveNotNeededBrackets)
		{
			Assert::AreEqual(explib::sortExpression("()"), std::string(""));
		}

		TEST_METHOD(RemoveNotNeededBrackets2)
		{
			Assert::AreEqual(explib::sortExpression("() + B"), std::string("B"));
		}

		TEST_METHOD(NotNeededBracketsInBrackets)
		{
			Assert::AreEqual(explib::sortExpression("(() + () + A + (C + D))"), std::string("(A + (C + D))"));
		}

		TEST_METHOD(DifferentSymbolCases)
		{
			Assert::AreEqual(explib::sortExpression("a + A + C + c + (B + b)"), std::string("(a + A + (B + b) + C + c)"));
		}

		TEST_METHOD(WrongSymbol)
		{
			Assert::ExpectException<std::runtime_error>([]
			{
				explib::sortExpression("((% + () + A + (C + D))");
			});
		}

		TEST_METHOD(WrongOperator)
		{
			Assert::ExpectException<std::runtime_error>([]
			{
				explib::sortExpression("(A ++ D)");
			});
		}

		TEST_METHOD(NoCloseBracket)
		{
			Assert::ExpectException<std::runtime_error>([]
			{
				explib::sortExpression("(A + D");
			});
		}

		TEST_METHOD(NoOpenBracket)
		{
			Assert::ExpectException<std::runtime_error>([]
			{
				explib::sortExpression("A + D)");
			});
		}

		TEST_METHOD(ElementAfterElement)
		{
			Assert::ExpectException<std::runtime_error>([]
			{
				explib::sortExpression("A + DD)");
			});
		}
	};
}