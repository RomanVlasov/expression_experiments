// ExpressionProcessor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <tuple>
#include <string>
#include <fstream>
#include <ios>
#include <iostream>
#include <iomanip>
#include "../explib/Explib.h"


namespace
{
	std::string parseInput(int argc, char* argv[])
	{
		if (argc < 2)
		{
			throw std::runtime_error("You need at least one command-line argument. Name of input file.");
		}

		return argv[1];
	}

	std::string generateOutputFilename(const std::string& inputFilename)
	{
		//Add file extention here
		static auto CONF_EXTENSIONS = { "txt", "conf", "test" };
		static auto SORTED_STR = "_sorted";

		const auto number = inputFilename.find_last_of(".");
		const auto ext = inputFilename.substr(number + 1);

		if (std::find(std::begin(CONF_EXTENSIONS), std::end(CONF_EXTENSIONS), ext) != std::end(CONF_EXTENSIONS))
		{
			return std::string(inputFilename.begin(), inputFilename.begin() + number) + SORTED_STR + "." + ext;
		}

		//Case of unknown or empty extension
		//It should work correctly with relative pathes "../test_cases"
		return inputFilename + SORTED_STR;
	}
}

int main(int argc, char* argv[])
{
	try
	{
		const auto inputFile = parseInput(argc, argv);
		std::cout << "Running tests from '" << inputFile << "'." << std::endl;

		const auto newfilename = generateOutputFilename(inputFile);

		auto input = std::ifstream(inputFile);
		auto output = std::ofstream(newfilename);

		std::string testCase{};
		while (std::getline(input, testCase))
		{
			output << explib::sortExpression(testCase) << std::endl;
		}

		input.close();
		output.close();

		std::cout << "End running tests. Check: " << newfilename << "." << std::endl;
		
	}
	catch (const std::exception& error)
	{
		std::cerr << "Error catched: " << error.what() << std::endl;
		std::cerr << "Please, check input parameters and input data format" << std::endl;
	}

	system("Pause");
	return 0;
}


