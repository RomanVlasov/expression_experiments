#include "Explib.h"

#include <sstream>
#include "element/Element.h"
#include "parser/Parser.h"

namespace explib
{
	std::string sortExpression(const std::string& originalExpression)
	{
		auto istream = std::istringstream{ originalExpression };
		auto parser = Parser{};
		auto element = parser.readElement(istream, false);
		if (element)
		{
			element->sort();
			return element->getExpression();
		}

		//Empty brackets "()" case
		return std::string("");
	}
}
