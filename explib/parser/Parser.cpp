#include "Parser.h"
#include <tuple>
#include <deque>
#include "ParserState.h"
#include "../element/ContainerElement.h"


namespace explib
{
	ElementPtr Parser::readElement(InputStream& istream, bool afterOpenBracket) const
	{
		auto env = ParserState::Environment{istream};
		env.bracketsMode = afterOpenBracket;
		auto state = ParserStatePtr{ std::make_unique<StartingState>() };
		auto parsedElements = std::deque<ElementPtr>{};

		while (state)
		{
			auto newElement = ElementPtr{};
			std::tie(state, newElement) = state->parse(env);

			if (newElement)
			{
				parsedElements.push_back(newElement);
			}
		}

		switch (parsedElements.size())
		{
		case 0:
			return {};
		case 1:
			//No brackets for single element
			return parsedElements.back();
		default:
			return std::make_shared<ContainerElement>(std::move(parsedElements));
		}
	}
}