#pragma once
#include "../ExplibDefines.h"


namespace explib
{
	using ParserStatePtr = std::shared_ptr<class ParserState>;

	class ParserState
	{
	public:
		using NextStateAndElement = std::pair<ParserStatePtr, ElementPtr>;
		struct Environment
		{
			bool bracketsMode;
			InputStream& istream;
			SymbolType currentSymbol;

			Environment(InputStream& inputstream) 
				: istream(inputstream)
				, bracketsMode(false)
				, currentSymbol(std::char_traits<SymbolType>::eof()) 
			{}
		};

		NextStateAndElement parse(Environment& env) const;

	protected:
		virtual NextStateAndElement parseAlphaSymbol(Environment& env) const = 0;
		virtual NextStateAndElement parsePlusSymbol(Environment& env) const = 0;
		virtual NextStateAndElement parseOpenBracket(Environment& env) const = 0;
		virtual NextStateAndElement parseCloseBracket(Environment& env) const = 0;
		virtual NextStateAndElement parseEndOfStream(Environment& env) const = 0;

		NextStateAndElement parseAlphaSymbolNormally(Environment& env) const;
		NextStateAndElement parseOpenBracketNormally(Environment& env) const;
		NextStateAndElement parseCloseBracketNormally(Environment& env) const;
		NextStateAndElement raiseError(Environment& env) const;
		NextStateAndElement parseEndOfStreamNormally(Environment& env) const;
	};


	class StartingState : public ParserState
	{
	private:
		virtual NextStateAndElement parseAlphaSymbol(Environment& env) const override;
		virtual NextStateAndElement parsePlusSymbol(Environment& env) const override;
		virtual NextStateAndElement parseOpenBracket(Environment& env) const override;
		virtual NextStateAndElement parseCloseBracket(Environment& env) const override;
		virtual NextStateAndElement parseEndOfStream(Environment& env) const override;
	};

	class AfterElementState : public ParserState
	{
	private:
		virtual NextStateAndElement parseAlphaSymbol(Environment& env) const override;
		virtual NextStateAndElement parsePlusSymbol(Environment& env) const override;
		virtual NextStateAndElement parseOpenBracket(Environment& env) const override;
		virtual NextStateAndElement parseCloseBracket(Environment& env) const override;
		virtual NextStateAndElement parseEndOfStream(Environment& env) const override;
	};

	class AfterPlusState : public ParserState
	{
	private:
		virtual NextStateAndElement parseAlphaSymbol(Environment& env) const override;
		virtual NextStateAndElement parsePlusSymbol(Environment& env) const override;
		virtual NextStateAndElement parseOpenBracket(Environment& env) const override;
		virtual NextStateAndElement parseCloseBracket(Environment& env) const override;
		virtual NextStateAndElement parseEndOfStream(Environment& env) const override;
	};


}