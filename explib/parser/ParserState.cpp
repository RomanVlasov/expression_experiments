#include "ParserState.h"
#include <istream>
#include <iterator>
#include <cctype>
#include <string>
#include "../element/SimpleElement.h"
#include "Parser.h"

namespace explib
{

	ParserState::NextStateAndElement ParserState::parse(Environment& env) const
	{
		const auto eos = std::istream_iterator<SymbolType>{};
		auto iter = std::istream_iterator<SymbolType>{env.istream};

		///////////////////////////////////////////
		// Let's just skip spaces
		///////////////////////////////////////////
		while (iter != eos && std::isspace(*iter))
		{
			++iter;
		}

		if (iter == eos)
		{
			return parseEndOfStream(env);
		}

		env.currentSymbol = *iter;

		if (std::isalpha(*iter))
		{
			return parseAlphaSymbol(env);
		}

		if (*iter == SymbolType{ '(' })
		{
			return parseOpenBracket(env);
		}

		if (*iter == SymbolType{ ')' })
		{
			return parseCloseBracket(env);
		}

		if (*iter == SymbolType{ '+' })
		{
			return parsePlusSymbol(env);
		}

		return raiseError(env);
	}


	////////////////////////////////////////////////////////////////////////////
	// ParserState
	////////////////////////////////////////////////////////////////////////////
	ParserState::NextStateAndElement ParserState::parseAlphaSymbolNormally(Environment& env) const
	{
		return std::make_pair(std::make_shared<AfterElementState>(), std::make_shared<SimpleElement>(env.currentSymbol));
	}

	ParserState::NextStateAndElement ParserState::parseOpenBracketNormally(Environment& env) const
	{
		auto parser = Parser{};
		auto element = parser.readElement(env.istream, true);
		return std::make_pair(std::make_unique<AfterElementState>(), element);
	}

	ParserState::NextStateAndElement ParserState::parseCloseBracketNormally(Environment& env) const
	{
		return env.bracketsMode ? NextStateAndElement{} : raiseError(env);
	}

	ParserState::NextStateAndElement ParserState::parseEndOfStreamNormally(Environment& env) const
	{
		return env.bracketsMode ? raiseError(env) : NextStateAndElement();
	}

	ParserState::NextStateAndElement ParserState::raiseError(Environment& env) const
	{
		throw std::runtime_error("Parsing error at position" + std::to_string(env.istream.tellg()));
		return{ {}, {} };
	}

	////////////////////////////////////////////////////////////////////////////
	// StartingState
	////////////////////////////////////////////////////////////////////////////
	StartingState::NextStateAndElement StartingState::parseAlphaSymbol(Environment& env) const
	{
		return parseAlphaSymbolNormally(env);
	}

	StartingState::NextStateAndElement StartingState::parsePlusSymbol(Environment& env) const
	{
		return raiseError(env);
	}

	StartingState::NextStateAndElement StartingState::parseOpenBracket(Environment& env) const
	{
		return parseOpenBracketNormally(env);
	}

	StartingState::NextStateAndElement StartingState::parseCloseBracket(Environment& env) const
	{
		return parseCloseBracketNormally(env);
	}

	StartingState::NextStateAndElement StartingState::parseEndOfStream(Environment& env) const
	{
		return parseEndOfStreamNormally(env);
	}


	////////////////////////////////////////////////////////////////////////////
	// AfterElementState
	////////////////////////////////////////////////////////////////////////////
	AfterElementState::NextStateAndElement AfterElementState::parseAlphaSymbol(Environment& env) const
	{
		return raiseError(env);
	}

	AfterElementState::NextStateAndElement AfterElementState::parsePlusSymbol(Environment& env) const
	{
		return std::make_pair(std::make_unique<AfterPlusState>(), ElementPtr());
	}

	AfterElementState::NextStateAndElement AfterElementState::parseOpenBracket(Environment& env) const
	{
		return raiseError(env);
	}

	AfterElementState::NextStateAndElement AfterElementState::parseCloseBracket(Environment& env) const
	{
		return parseCloseBracketNormally(env);
	}

	AfterElementState::NextStateAndElement AfterElementState::parseEndOfStream(Environment& env) const
	{
		return parseEndOfStreamNormally(env);
	}


	////////////////////////////////////////////////////////////////////////////
	// AfterPlusState
	////////////////////////////////////////////////////////////////////////////
	AfterPlusState::NextStateAndElement AfterPlusState::parseAlphaSymbol(Environment& env) const
	{
		return parseAlphaSymbolNormally(env);
	}

	AfterPlusState::NextStateAndElement AfterPlusState::parsePlusSymbol(Environment& env) const
	{
		return raiseError(env);
	}

	AfterPlusState::NextStateAndElement AfterPlusState::parseOpenBracket(Environment& env) const
	{
		return parseOpenBracketNormally(env);
	}

	AfterPlusState::NextStateAndElement AfterPlusState::parseCloseBracket(Environment& env) const
	{
		return raiseError(env);
	}

	AfterPlusState::NextStateAndElement AfterPlusState::parseEndOfStream(Environment& env) const
	{
		return raiseError(env);
	}

}