#pragma once
#include "../element/Element.h"

#include <memory>
#include "../ExplibDefines.h"

namespace explib
{
	class Parser
	{
	public:
		ElementPtr readElement(InputStream& istream, bool afterOpenBracket) const;

	};
}