#pragma once
#include <iosfwd>
#include <memory>
#include <string>


namespace explib
{
	using SymbolType = char;
	using InputStream = std::basic_istream<SymbolType>;
	using StringType = std::basic_string<SymbolType>;
	using ElementPtr = std::shared_ptr<class Element>;
}