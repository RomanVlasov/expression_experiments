#pragma once
#include "Element.h"

#include <deque>

namespace explib
{
	class ContainerElement : public Element
	{
	public:
		using ElementCollection = std::deque<ElementPtr>;

	public:
		template <typename CollectionT>
		ContainerElement(CollectionT&& collection)
			: collection_(std::forward<CollectionT>(collection))
		{

		}

		virtual SymbolType doGetObjectToSort() const override;
		virtual void doSort() override;
		virtual StringType doGetExpression() const override;

	private:
		ElementCollection collection_;
	};
}