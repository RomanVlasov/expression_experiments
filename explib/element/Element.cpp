#include "Element.h"


namespace explib
{

	SymbolType Element::getObjectToSort() const
	{
		return doGetObjectToSort();
	}

	StringType Element::getExpression() const
	{
		return doGetExpression();
	}

	void Element::sort()
	{
		doSort();
	}

}
