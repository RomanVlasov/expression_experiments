#include "ContainerElement.h"
#include <algorithm>
#include <cctype>


namespace explib
{
	SymbolType ContainerElement::doGetObjectToSort() const
	{
		if (collection_.empty())
		{
			return std::char_traits<SymbolType>::eof();
		}

		return collection_.front()->getObjectToSort();
	}

	void ContainerElement::doSort()
	{
		for (auto& elem : collection_)
		{
			elem->sort();
		}

		std::sort(collection_.begin(), collection_.end(), 
			[](ElementPtr elem1, ElementPtr elem2) -> bool
		{
			if (!elem1 || !elem2)
			{
				//Just in case
				return false;
			}

			return std::toupper(elem1->getObjectToSort()) < std::toupper(elem2->getObjectToSort());
		});
	}

	StringType ContainerElement::doGetExpression() const
	{
		//TODO: Think about wstring
		//Maybe, make separate static class for control symbols 
		//For example, Control::openBracket()

		auto mainPart = StringType{};
		for (const auto& elem : collection_)
		{
			if (!mainPart.empty())
			{
				mainPart += " + ";
			}
			mainPart += elem->getExpression();
		}
		return "(" + mainPart + ")";
	}
}