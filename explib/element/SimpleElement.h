#pragma once
#include "Element.h"

namespace explib
{
	class SimpleElement : public Element
	{
	public:
		SimpleElement(SymbolType symbol);

	private:
		virtual SymbolType doGetObjectToSort() const override;
		virtual void doSort() override {}
		virtual StringType doGetExpression() const override;

	private:
		SymbolType data_;
	};
}