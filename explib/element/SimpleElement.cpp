#include "SimpleElement.h"

namespace explib
{
	SimpleElement::SimpleElement(SymbolType symbol)
		: data_(symbol)
	{

	}

	SymbolType SimpleElement::doGetObjectToSort() const
	{
		return data_;
	}

	StringType SimpleElement::doGetExpression() const
	{
		//TODO: Think about other StringType
		return std::string(1, data_);
	}
}