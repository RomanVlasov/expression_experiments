#pragma once
#include <iosfwd>
#include "../ExplibDefines.h"

namespace explib
{
	class Element
	{
	public:
		Element() = default;
		virtual ~Element() = default;

		SymbolType getObjectToSort() const;
		StringType getExpression() const;
		void sort();

	protected:
		virtual SymbolType doGetObjectToSort() const = 0;
		virtual void doSort() = 0;
		virtual StringType doGetExpression() const = 0;
	};
}
