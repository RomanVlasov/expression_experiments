// RandomExpressionGenerator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <tuple>
#include <string>
#include <fstream>
#include <iostream>
#include <random>
#include <ctime>
#include <algorithm>
#include <math.h>
#include "../explib/element/SimpleElement.h"
#include "../explib/element/ContainerElement.h"

namespace
{

	static auto generator = std::mt19937{ static_cast<unsigned int>(std::time(0)) };

	////////////////////////////////////////////////
	//Helper functions
	////////////////////////////////////////////////
	unsigned int rand(unsigned int min, unsigned int max)
	{
		return std::uniform_int_distribution<unsigned int>(min, max)(generator);
	}

	char rand(char min, char max)
	{
		return static_cast<char>(std::uniform_int_distribution<int>(static_cast<int>(min), static_cast<int>(max))(generator));
	}

	////////////////////////////////////////////////////////////
	//Generate single element of maximum length - maxLength
	////////////////////////////////////////////////////////////
	explib::ElementPtr generateElement(unsigned int maxLength)
	{
		auto depth = rand(0, maxLength);
		if (depth == 0)
		{
			return std::make_shared<explib::SimpleElement>(rand('A', 'Z'));
		}

		if (depth == 1)
		{
			return std::make_shared<explib::SimpleElement>(rand('a', 'z'));
		}

		auto collection = explib::ContainerElement::ElementCollection(depth);
		for (unsigned int i = 0; i < depth; i++)
		{
			collection[i] = generateElement(std::max(unsigned int{ 0 }, maxLength - 1));
		}
		return std::make_shared<explib::ContainerElement>(collection);
	}

	////////////////////////////////////////////////////////////
	//Generate random test case
	////////////////////////////////////////////////////////////
	std::string generateTestCase(unsigned int maxLength)
	{
		auto firstLevelDistribution = std::uniform_int_distribution<unsigned int>{ 1, maxLength };

		const auto length = firstLevelDistribution(generator);
		auto collection = explib::ContainerElement::ElementCollection(length);
		for (unsigned int i = 0; i < length; i++)
		{
			collection[i] = generateElement(rand(0, maxLength));
		}

		const auto element = std::make_shared<explib::ContainerElement>(std::move(collection));
		return element->getExpression();
	}

	////////////////////////////////////////////////////////////
	//Generate test strings and write them to output file
	////////////////////////////////////////////////////////////
	void generateTestStrings(unsigned int number, std::fstream& fs)
	{
		//Magic formula to enlarge length of generated elements after enlarging total number of test cases
		//TODO: Read proper literature
		//For example, http://www.mii.lt/olympiads_in_informatics/pdf/INFOL055.pdf
		const auto maxLength = 6 + number / 45;

		for (auto i = unsigned int{ 0 }; i < number; i++)
		{
			fs << generateTestCase(maxLength) << std::endl;
		}
	}

	////////////////////////////////////////////////////////////
	//Parse input
	////////////////////////////////////////////////////////////
	std::pair<unsigned int, std::string> parseInput(int argc, char* argv[])
	{
		if (argc < 3)
		{
			throw std::runtime_error("You need at least two command-line arguments. Number of strings and output file.");
		}

		return std::make_pair(std::stoul(argv[1]), argv[2]);
	}
}


int main(int argc, char* argv[])
{
	try
	{
		const auto params = parseInput(argc, argv);
		std::cout << "Start test generating." << std::endl;

		std::fstream fs(params.second, std::fstream::out);
		generateTestStrings(params.first, fs);
		fs.close();

		std::cout << params.first << " test cases were generated in " << params.second << "." << std::endl;
	}
	catch (const std::exception& error)
	{
		std::cerr << "Error catched: " << error.what() << std::endl;
		std::cerr << "Please, check input parameters" << std::endl;
	}
	
	system("Pause");
	return 0;
}

